import org.mockserver.client.server.MockServerClient;
import org.mockserver.model.Delay;
import org.mockserver.model.Header;
import org.mockserver.model.Parameter;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.mockserver.matchers.Times.exactly;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;

/**
 * Created by marcoollivier on 28/11/16.
 */
public class Main {

    public static void main(String[] args) {
        MockServerClient mockServerClient = new MockServerClient("127.0.0.1", 1080);
        mockServerClient.when(
                        request()
                                .withMethod("POST")
                                .withPath("/login")
                                .withQueryStringParameters(
                                        new Parameter("returnUrl", "/account")
                                )
                                .withBody("{ 'msisdn': '2977778899', 'email':'kenpachi@soulsociety.com', 'nome':'kenpachi', 'origem':'SMS', 'cpf':'122977778899', 'pwd':'123456', 'dataDeNascimento':'08/03/1990', 'token':'F0AAB4FF-534C-4883-8CC4-13E2B271FA37' }"),
                        exactly(2)
                )
                .respond(
                        response()
                                .withStatusCode(200)
                                .withHeaders(
                                        new Header("Content-Type", "application/json; charset=utf-8"),
                                        new Header("Cache-Control", "public, max-age=86400")
                                )
                                .withBody("{'status':'SUCESSO','uuid':'ce4d4f64-2dde-440c-b5bb-1a298bde7be2', 'constraints':[]}")
                                .withDelay(new Delay(SECONDS, 1))
                );

    }

}
